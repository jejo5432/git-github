#!/bin/sh
#
#
# Description:
# Validate the disk space of the file system and will trigger the alert if it running out of space .
# Validate the cpu idle time and memory usage of the server
# Written by : Raj Kumar J (rajkumar.j1@photoninfotech.net)
#
# Email Flags
NO_EMAIL=0
THRESHOLD_LEVEL=90
MEMORY_THRESHOLD_LEVEL=80
CPU_IDLE_THRESHOLD_LEVEL=75
CPU_USAGE_THRESHOLD_LEVEL=4
ROOT_DIRECTORY='root'
HOME_DIRECTORY='/home'
CPU_USER='wcsuser'
BODY_TEXT='Server Alerts'
MAIL_TO='rajkumar.j1@photoninfotech.net,anish.k@photoninfotech.net,nitin.rathee@photoninfotech.net,ramkumar.r1@photoninfotech.net,ajay.dorai@photoninfotech.net,arvindhan.lakshminarayanan@photoninfotech.net,william.erram@photoninfotech.net,manikandan.ar@photoninfotech.net,kisan.kumar@photoninfotech.net,udaykumar.malireddi@photoninfotech.net'


failiure_email(){
    DIRECTORY=$1
    CURRENT_UTIL=$2 
    echo $BODY_TEXT | mutt -s "Alert : Server Alerts,\n Current utilization is $CURRENT_UTIL" $MAIL_TO
}

CURRENT_ROOT_USAGE=`df -h | grep '/dev/xvda1' | awk '{ print $5 }'`
CURRENT_ROOT_USAGE=`echo $CURRENT_ROOT_USAGE | sed 's/%//'`

CURRENT_HOME_USAGE=`df -h | grep '/dev/xvda2' | awk '{ print $5 }'`
CURRENT_HOME_USAGE=`echo $CURRENT_HOME_USAGE | sed 's/%//'`

CURRENT_MEMORY_USAGE=$(free -m | awk 'NR==2{printf "%.2f%%\t\t", $3*100/$2 }')
CURRENT_MEMORY_USAGE=`echo $CURRENT_ROOT_USAGE `

CURRENT_CPU_IDLE=$(top -n 1 -b -u wcsuser | grep '%Cpu(s)' | awk '{ print $8}')
CURRENT_CPU_IDLE=`echo $CURRENT_HOME_USAGE | sed 's/%//'`

CURRENT_CPU_USAGE=$(top -n 1 -b -u wcsuser | grep '%Cpu(s)' | awk '{ print $2}')
CURRENT_CPU_USAGE=`echo $CURRENT_HOME_USAGE | sed 's/%//'`


echo $ROOT_DIRECTORY
echo $CURRENT_ROOT_USAGE
echo $HOME_DIRECTORY
echo $CURRENT_HOME_USAGE
echo $CURRENT_CPU_IDLE
echo $CURRENT_MEMORY_USAGE

if [[ $CURRENT_ROOT_USAGE -ge $THRESHOLD_LEVEL ]]; then
    
    failiure_email $ROOT_DIRECTORY $CURRENT_USAGE //This is to find if the disk space crosses 90% in root directory

elif [[ $CURRENT_HOME_USAGE -ge $THRESHOLD_LEVEL ]]; then

    failiure_email $ROOT_DIRECTORY $CURRENT_USAGE // This is to find if the disk space crosses 90% in application directory
	
elif [[ $CURRENT_MEMORY_USAGE -ge $MEMORY_THRESHOLD_LEVEL ]]; then

    failiure_email $ROOT_DIRECTORY $CURRENT_MEMORY_USAGE // This is to find if the Server Memory crosses 80%
	
elif [[ $CURRENT_CPU_IDLE -le $CPU_IDLE_THRESHOLD_LEVEL ]]; then

    failiure_email $CPU_USER $CURRENT_CPU_IDLE // This is to find if the wcsuser CPU Idle time goes down and it will throw alert
	
elif [[ $CURRENT_CPU_USAGE -le $CPU_USAGE_THRESHOLD_LEVEL ]]; then

    failiure_email $CPU_USER $CURRENT_CPU_USAGE // This is to find if the wcsuser CPU usage time goes down and it will throw alert
	
else
    echo "We are good now !!"
fi

