#!/bin/bash
#
# Copyright 2017, Sirius
#
# Created 2017-12-01 by Kaleeswaran Kalimuthu <kaleeswaran.kalimuthu@siriuscom.com>
#
# Description:


############ FUNCTIONS ####################

print_help() {

        echo -e "Usage :"
        echo -e " $PROGNAME ENV"
        exit 1
}

############ MAIN #######################

CURRENT_TIME=`date`
echo -e "Execution started at $CURRENT_TIME"
PROGNAME=`basename $0`

if [[ $# -lt 1 ]]; then
        echo -e "Please pass arguments required for the execution of the script - the environment is required"
        print_help
	exit 1
else
        ENV=$1
fi

ENV=`echo $ENV | tr [:upper:] [:lower:]`

case $ENV in
        dr_stage|stage )
		cd /opt/IBM/WebSphere/CommerceServer80/wcbd
	        ./syncSolr_Stg.sh
                ;;
        dr_cell2|prod_cell2 )
		cd /opt/IBM/WebSphere/CommerceServer80/wcbd
		./syncSolr_Prd_Cell02.sh
                ;;
        dr_cell1|prod_cell1 )
		cd /opt/IBM/WebSphere/CommerceServer80/wcbd
		./syncSolr_Prd_Cell01.sh
                ;;
        * )
                echo -e "Please specify the correct environment parameter"
                print_help
		exit 1
esac
