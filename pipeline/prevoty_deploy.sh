#!/bin/bash
#
# Copyright 2017, Sirius
#
# Created 2017-12-06 by Kaleeswaran Kalimuthu <kaleeswaran.kalimuthu@siriuscom.com>
#
# Description:
# This script was created to deploy the Prevoty jar to all the app tiers.

############ FUNCTIONS ####################

print_help() {

        echo -e "Usage :"
        echo -e " $PROGNAME EnvName"
        exit 1
}

CURRENT_TIME=`date`
echo -e "Execution started at $CURRENT_TIME"

PROGNAME=`basename $0`

# GET ENV DETAILS
if [[ $# -lt 1 ]]; then
        echo -e "Please pass at least one arguments required for the execution of the script - the environment is required to run this script"
        print_help
	exit 1
else
        ENV=$1
fi
# CONVERT TO LOWER CASE
ENV=`echo $ENV | tr [:upper:] [:lower:]`
# LOAD ENV SPECIFIC CONF FILE
if [[ ! -f /home/wasadm/bin/pipeline/conf/${ENV}.conf ]]; then
        echo -e "Missing pipeline.conf file. Please check and re-run!!"
        exit 1
else
        . /home/wasadm/bin/pipeline/conf/${ENV}.conf
fi
# PREVOTY LOGIC TO SCP files to all cluster members
PREVOTY_DIR="/opt/IBM/WebSphere/CommerceServer80/wcbd/source/build/workspace/Artifacts/Prevoty"
echo "Envname $ENV"
case $ENV in
        psqa|dev2|dev3|stgqa|prdqa|dr_stage|dr_cell1|dr_cell2|stage|prod_cell2|prod_cell1 )
                echo -e "Constructing server cluster array name"
                ENV_prevoty_serverCluster_nw="${ENV}_prevoty_serverCluster"[@]
                ENV_prevoty_serverCluster=( "${!ENV_prevoty_serverCluster_nw}" )

                for wcsServer in "${!ENV_prevoty_serverCluster[@]}"
                do
                        wcsServer="${ENV_prevoty_serverCluster[$wcsServer]}"
                        echo -e "Logging into $wcsServer now"
                        ssh wasadm@$wcsServer </dev/null <<EOF 2>&1
                        hostname
                        if [[ ! -d /opt/IBM/Prevoty/ ]]; then
                                echo "/opt/IBM/Prevoty/ - directory is missing, creating it now"
                                mkdir -p /opt/IBM/Prevoty/
				if [[ $? -eq "0" ]]; then
					echo "created /opt/IBM/Prevoty directory"
				else 
					echo "unable to create directory /opt/IBM/Prevoty. Check directory permissions for ID"
					exit 1
				fi
                        fi
                        if [[ ! -d /opt/Logs/Prevoty/ ]]; then
                                echo "/opt/Logs/Prevoty/ - directory is missing, creating it now"
                                mkdir -p /opt/Logs/Prevoty/
				if [[ $? -eq "0" ]]; then
                                        echo "created /opt/Logs/Prevoty directory"
                                else
                                        echo "unable to create directory /opt/Logs/Prevoty. Check directory permissions for ID"
                                        exit 1
                                fi
                        fi
                        rm -f /opt/IBM/WebSphere/AppServer/lib/ext/prevoty-rasp-*.jar
                        rm -f /opt/IBM/Prevoty/prevoty-*.jar

EOF
                continue
                done

                for wcsServer in "${!ENV_prevoty_serverCluster[@]}"
                do
                        wcsServer="${ENV_prevoty_serverCluster[$wcsServer]}"
                        echo "GET BUILD server based on conf file: $prevotyBuildServer"
                        scp -p $PREVOTY_DIR/*.jar wasadm@${wcsServer}:/opt/IBM/Prevoty/ </dev/null
                        if [[ $? -eq "0" ]]; then
                                echo "File Transfer completed successfully to ${wcsServer}"
                        else
                                echo "File Transfer to ${wcsServer} was not successful"
                                exit 1
                        fi
		done

                for wcsServer in "${!ENV_prevoty_serverCluster[@]}"
                do
                        wcsServer="${ENV_prevoty_serverCluster[$wcsServer]}"
                        echo -e "Logging into $wcsServer now"
                        ssh wasadm@$wcsServer </dev/null <<EOF 2>&1
                        hostname
                        cp -r /opt/IBM/Prevoty/prevoty-rasp-*.jar /opt/IBM/WebSphere/AppServer/lib/ext/

                        echo -e "Creating sym link now"
                        ln -sf /opt/IBM/Prevoty/prevoty-agent-*.jar /opt/IBM/Prevoty/prevoty-agent.jar
                        if [[ $? -eq "0" ]]; then
                                echo "SymLink completed successfully"
                        else
                                echo "SymLink creation was not successful"
                                exit 1
                        fi
EOF
                continue
                done
                ;;
        * )
                echo "check the ENV variable passed and verify the appropriate configuration file exists"
		exit 1
esac
