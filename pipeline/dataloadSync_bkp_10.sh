#!/bin/bash
#
# Copyright 2017, Sirius
#
# Created 2017-12-01 by Kaleeswaran Kalimuthu <kaleeswaran.kalimuthu@siriuscom.com>
#
# Description:


############ FUNCTIONS ####################

print_help() {

        echo -e "Usage :"
        echo -e " $PROGNAME ENV"
        exit 1
}

############ MAIN #######################

CURRENT_TIME=`date`
echo -e "Execution started at $CURRENT_TIME"
PROGNAME=`basename $0`

if [[ $# -lt 1 ]]; then
        echo -e "Please pass arguments required for the execution of the script - the environment is required"
        print_help
	exit 1
else
        ENV=$1
fi

ENV=`echo $ENV | tr [:upper:] [:lower:]`

case $ENV in
        stage|dr_stage )
		cd /opt/IBM/WebSphere/CommerceServer80/wcbd
	        ./syncDataload_Stg.sh
                ;;
        prod_cell2|dr_cell2 )
		echo "dataload sync runs only for one cell in prod"
                ;;
        prod_cell1|dr_cell1 )
		echo "dataload sync runs in prod only for employee loads and needs to be done manually"
                ;;
        * )
                echo -e "Please specify the correct environment parameter"
                print_help
		exit 1
esac
