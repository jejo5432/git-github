#load branch and tag details firest
CURRENT_TIME=`date`
echo -e "Execution started at $CURRENT_TIME"

if [[ $# -lt 4 ]]; then
        echo -e "Please pass all arguments required for the execution of the script"
        exit 1
else
        BRANCH_NAME=$1
        TAG=$2
	SVN_USER=$3
	SVN_PASSWORD=$4
fi

# Run the build
cd /opt/IBM/WebSphere/CommerceServer80/wcbd/

./svn-update-with-password.sh ${BRANCH_NAME} ${TAG} ${SVN_USER} ${SVN_PASSWORD}
if [[ $? -eq "0" ]]; then
        echo "svn update with password done"
else
	echo "check on svn update with password error"
	exit 1
fi

./wcbd-ant -buildfile pvh-build.xml -v -Dbuild.label=${TAG} -Dbuild.type=search -Dbuild.branch=${BRANCH_NAME}
if [[ $? -eq "0" ]]; then
        echo "search build done"
else
	echo "check on search build error"
	exit 1
fi

./wcbd-ant -buildfile pvh-build.xml -v -Dbuild.label=${TAG} -Dbuild.type=wc -Dbuild.branch=${BRANCH_NAME}
if [[ $? -eq "0" ]]; then
        echo "WC build done"
else
        echo "check on WC build error"
        exit 1
fi
