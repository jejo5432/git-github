#!/bin/bash
#
# Copyright 2017, Sirius
#
# Created 2017-12-01 by Kaleeswaran Kalimuthu <kaleeswaran.kalimuthu@siriuscom.com>
#
# Description:
# This script was created to enable, disable and execute the rundeck job by terminal .


############ FUNCTIONS ####################

print_help() {

        echo -e "Usage :"
        echo -e " $PROGNAME --enable ENV"
        echo -e " $PROGNAME --disable ENV"
        echo -e " $PROGNAME --execute ENV EXECUTE_JOB_ID"
        exit 1
}

disableRundeckJobs() {

        echo -e "Disabling the deployment depend rundeck jobs"
        JOBID=$1

        if [[ ! -z ${JOBID} ]]; then
		URL="${RUNDECK_API_URL}/job/${JOBID}/schedule/disable"
                curl -H "X-RunDeck-Auth-Token:${RUNDECK_AUTH_TOKEN}" -X POST "${URL}" > output.txt
                STATUS=`head -1 output.txt | cut -d ">" -f 2 | cut -d "<" -f 1`

                if [[ "$STATUS" == "true" ]]; then
                        echo -e "${JOBID} - Disabled"
                else
                        echo -e "${JOBID} - Disable was not successfully , Please disable manually !!"
			exit 1
                fi
        else
                echo -e "Missing parameters JOBID"
		exit 1
        fi
	rm -rf output.txt

}

enableRundeckJobs() {

        echo -e "Enabling the deployment depend rundeck jobs"
        JOBID=$1
        OUTPUT=""

        if [[ ! -z ${JOBID} ]]; then
                URL="${RUNDECK_API_URL}/job/${JOBID}/schedule/enable"
                curl -H "X-RunDeck-Auth-Token:${RUNDECK_AUTH_TOKEN}" -X POST "${URL}" > output.txt
		STATUS=`head -1 output.txt | cut -d ">" -f 2 | cut -d "<" -f 1`

                if [[ "$STATUS" == "true" ]]; then
                        echo -e "${JOBID} - Enabled"
                else
                        echo -e "${JOBID} - Enable was not successfully , Please enable manually !!"
			exit 1
                fi
        else
                echo -e "Missing parameters JOBID"
		exit 1
        fi
	rm -rf output.txt
}

executeRundeckJobs() {

        JOBID=$1
        echo -e "Executing the jobs by ${JOBID}"

        if [[ ! -z ${JOBID} ]]; then
		URL="${RUNDECK_API_URL}/job/${JOBID}/run"
                curl -H "X-RunDeck-Auth-Token:${RUNDECK_AUTH_TOKEN}" -X POST "${URL}" > execute_status.xml

                EXEC_ID=`grep "<execution id=" execute_status.xml | awk '{print $2}' | grep -o "[0-9]\+"`
                echo "Job execution id is $EXEC_ID"
                if [[ ! -z $EXEC_ID ]]; then
                        EXEC_STATUS=1
                        while [[ $EXEC_STATUS -eq 1 ]]; do
				URL="${RUNDECK_API_URL}/job/${JOBID}/run"
				curl -H "X-RunDeck-Auth-Token:$RUNDECK_AUTH_TOKEN" -X GET "${RUNDECK_API_URL}/execution/$EXEC_ID" > execute_status.xml
				#curl -H "X-RunDeck-Auth-Token:${RUNDECK_AUTH_TOKEN}" -X POST "${URL}" > execute_status.xml
                                STATUS=`grep 'execution' execute_status.xml| grep 'status' | grep -wo 'succeeded\|failed\|running\|killed\|aborted'`

                                echo "Job execution status is $STATUS"

                                if [[ "$STATUS" == "succeeded" ]]; then
                                        echo "JOBID - ${JOBID} - Successfully completed."
                                        EXEC_STATUS=0
                                elif [[ "$STATUS" == "failed" ]] || [[ "$STATUS" == "killed" ]] || [[ "$STATUS" == "aborted" ]]; then
                                        echo "JOBID - ${JOBID} - Failed or Killed"
                                        EXEC_STATUS=0
					exit 1
                                        # -> Need to confirm whether we need to send any email confirmation to the team or not
                                elif [[ "$STATUS" == "running" ]]; then
                                        echo "Still running - ${JOBID}"
                                        EXEC_STATUS=1
                                        sleep 60s
                                else
                                        echo "JOBID - ${JOBID} is in unknown state, Please review the logs and make sure the process is running fine!!"
                                        EXEC_STATUS=0
					exit 1
                                fi
                        done
                else
                        echo -e "JOBID-${JOBID} was not successfully executed,It might be already running!!"
			exit 1
                fi
		rm -rf execute_status.xml
        fi

}

############ MAIN #######################

DIR=/home/wasadm/bin/pipeline/conf

CURRENT_TIME=`date`
echo -e "Execution started at $CURRENT_TIME"

PROGNAME=`basename $0`
if [[ $# -lt 2 ]]; then
        echo -e "Please pass at least two arguments required for the execution of the script - the task and the environment and if required the job_id as well"
        print_help
	exit 1
else
        TASK=$1
        ENV=$2
fi

ENV=`echo $ENV | tr [:upper:] [:lower:]`
TASK=`echo $TASK | tr [:upper:] [:lower:]`

if [[ $# -eq 3 ]]; then
        execute_job_uid=$3
fi

case $ENV in
        local|stage|prod_cell2|prod_cell1|dr_stage|dr_cell1|dr_cell2 )
                if [[ ! -f $DIR/$ENV.conf ]]; then
                        echo -e "Missing configuration file ."
                        exit 1
                else
                        . $DIR/$ENV.conf
                fi
                ;;
        * )
                echo "check the ENV variable passed and verify the approproate configuration file exists"
		exit 1
esac

if [[ -z $TASK ]]; then
        echo -e "Please specify the parameters to enable/disable/execute"
        print_help
fi

case $TASK in
        --enable)
                for job_uid in "${RUNDECK_ENABLE_JOB_IDS[@]}" ; do
                        enableRundeckJobs ${job_uid}
                done
                ;;
        --disable)
                for job_uid in "${RUNDECK_DISABLE_JOB_IDS[@]}" ; do
			echo ${job_uid}
                        disableRundeckJobs ${job_uid}
                done
                ;;
        --execute)
                executeRundeckJobs ${execute_job_uid}
                ;;
        * )
                echo -e "Please specify the parameters to enable/disable/execute"
                print_help
		exit 1
esac
