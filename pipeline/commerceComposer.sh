#!/bin/bash
#
# Copyright 2017, Sirius
#
# Created 2017-12-06 by Kaleeswaran Kalimuthu <kaleeswaran.kalimuthu@siriuscom.com>
#
# Description:
# This script was created to restart servers on all the app tiers.

############ FUNCTIONS ####################

print_help() {

        echo -e "Usage :"
        echo -e " $PROGNAME EnvName"
        exit 1
}

CURRENT_TIME=`date`
echo -e "Execution started at $CURRENT_TIME"

PROGNAME=`basename $0`

# GET ENV DETAILS
if [[ $# -lt 1 ]]; then
        echo -e "Please pass at least one arguments required for the execution of the script - the environment is required to run this script"
        print_help
else
        ENV=$1
fi
# CONVERT TO LOWER CASE
ENV=`echo $ENV | tr [:upper:] [:lower:]`
# LOAD ENV SPECIFIC CONF FILE
if [[ ! -f /home/wasadm/bin/pipeline/conf/${ENV}.conf ]]; then
        echo -e "Missing pipeline.conf file. Please check and re-run!!"
        exit 1
else
        . /home/wasadm/bin/pipeline/conf/${ENV}.conf
fi
# PREVOTY LOGIC TO SCP files to all cluster members
echo "Envname $ENV"
case $ENV in
        dr_stage|stage )
                echo -e "Constructing server cluster array name"
                ENV_wcs_serverCluster_nw="${ENV}_wcs_serverCluster"[0]
                ENV_wcs_serverCluster=( "${!ENV_wcs_serverCluster_nw}" )
                ssh wasadm@$ENV_wcs_serverCluster 'chmod 755 /opt/dataload/CommerceComposer/run.sh; /opt/dataload/CommerceComposer/run.sh'
		if [[ $? -gt "0" ]]; then
        		echo "CommerceComposer dataload had failures. Please review before you proceed"
			exit 1
		else
			echo "commerce composer dataload was successful"
			exit 0
		fi
                ;;
        * )
                echo "check if commerce composer is to be run in this environment before re-running this. Contact admin team."
esac
