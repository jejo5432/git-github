#load branch and tag details firest
CURRENT_TIME=`date`
echo -e "Execution started at $CURRENT_TIME"

if [[ $# -lt 3 ]]; then
        echo -e "Please pass all arguments required for the execution of the script"
        exit 1
else
        TAG=$1
	deployENV=$2
	DELTA_MODE=$3
	# the fourth argument is not insisted upton as it is empty and it leads to failures.
	BASELINE_TAG=$4
fi

# Run the Deploy
. /home/b2cnaswc/.profile
if [[ $? -eq "0" ]]; then
        echo "loaded the b2cnaswc profile"
else
        echo "check if b2cnaswc profile exists"
        exit 1
fi

cd /opt/IBM/WebSphere/CommerceServer80/wcbd/
./deploy.sh ${TAG} ${deployENV} ${DELTA_MODE} ${BASELINE_TAG}
if [[ $? -eq "0" ]]; then
        echo "deploy completed"
else
	echo "verify deploy"
	exit 1
fi
