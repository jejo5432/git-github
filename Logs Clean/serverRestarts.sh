#! /bin/sh
#
# Copyright 2017, Sirius
#
# Created 2017-07-14 by Kaleeswaran Kalimuthu <kaleeswaran.kalimuthu@siriuscom.com>
#
# Description:
# This script will restart the whole cell servers
# Including AppServers,Webservers and Solr Servers
# Name : restartCellServers.sh
#
# Here we have four position parameters .
#
# CELL_NAME - Cell/ENV Needs to be restarted

print_usererror(){
    echo -e "Please make sure , You are running this deploy script as wasadm user
    Failed : Exit code 1"
}

ENV_NAME=`echo $1 | tr '[:upper:]' '[:lower:]'`

if [ ! -f ~/etc/commerce.conf-${ENV_NAME} ]; then
  echo "Configuration file ~/etc/commerce.conf is missing, cannot continue processing!"
  exit 1
fi

. ~/etc/commerce.conf-${ENV_NAME}


USER=`echo $USER`

# Validating whether we are running the script as wasadm user .
# if not it will not allow you to run this.

if [[ $USER != "wasadm" ]]; then
    print_usererror
    exit 1
fi
# Restarting App and Solrprofile simultaneously (which means the complete site will be down for a while)
echo -e "Restarting the $ENV now"
ssh ${USER}@${APP_HOSTNAME} ~/bin/serverRestart.sh ${WASPROFILE} ${WC_PROC} restart; ssh ${USER}@${SOLR_HOSTNAME} ~/bin/serverRestart.sh ${SOLR_MASTER} ${SOLR_MASTER_PROC} restart; ssh ${USER}@${SOLR_HOSTNAME} ~/bin/serverRestart.sh ${SOLR_SLAVE} ${SOLR_SLAVE_PROC} restart;
echo -e "Restart $ENV completed"
