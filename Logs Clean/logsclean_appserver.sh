WEBLOGS='/opt/IBM/HTTPServer/logs'
APP_LOGS='/opt/IBM/WebSphere/AppServer/profiles/B2CNAPR/logs/server1/'
AGENT_LOGS='/opt/IBM/WebSphere/AppServer/profiles/B2CNAPR/logs/nodeagent/'
COMMERCE='/opt/IBM/WebSphere/CommerceServer80/logs'



print_help(){
        cat <<EOF
please enter the age along with the age above which logs has to be deleted and
The age of the backup logs to be deleted
        example :- ./logscleaner.sh 3 30
        3 - zip all the logs older than 3 days
        30 - deletes all the zip files older than 30 days
EOF
}
ZIP=$1
DELETE=$2
echo $files_to_gzip;
if [  "$ZIP" == "" -o "$DELETE" == "" ]; then
        print_help
        exit 1
fi
#cd $WEBLOGS
#find . -type f -name "access_log-*" -mtime +$ZIP -exec gzip {} \;
#find . -type f -name "error_log-*" -mtime +$ZIP -exec gzip {} \;
#find . -type f -name "*.gz" -mtime +$DELETE -exec rm {} \;

cd $APP_LOGS
find . -type f -name "SystemOut*" -mtime +$ZIP -exec gzip {} \;
find . -type f -name "SystemErr*" -mtime +$ZIP -exec gzip {} \;
find . -type f -name "native_stdout*" -mtime +$ZIP -exec gzip {} \;
find . -type f -name "native_stderr*" -mtime +$ZIP -exec gzip {} \;
find . -type f -name "*.gz" -mtime +$DELETE -exec rm {} \;

cd $AGENT_LOGS
find . -type f -name "SystemOut*" -mtime +$ZIP -exec gzip {} \;
find . -type f -name "SystemErr*" -mtime +$ZIP -exec gzip {} \;
find . -type f -name "*.gz" -mtime +$DELETE -exec rm {} \;

cd $COMMERCE
find . -type f -name "NonATPInventory_ERROR*" -mtime +$ZIP -exec gzip {} \;
find . -type f -name "NonATPInventory_ERROR*.gz" -mtime +$DELETE -exec rm {} \;

find . -type f -name "FTPUpload_ERROR_*" -mtime +$ZIP -exec gzip {} \;
find . -type f -name "FTPUpload_ERROR_*.gz" -mtime +$DELETE -exec rm {} \;

find . -type f -name "Orders_ERROR*" -mtime +$ZIP -exec gzip {} \;
find . -type f -name "Orders_ERROR*.gz" -mtime +$DELETE -exec rm {} \;

find . -type f -name "Customers_ERROR*" -mtime +$ZIP -exec gzip {} \;
find . -type f -name "Customers_ERROR*.gz" -mtime +$DELETE -exec rm {} \;
