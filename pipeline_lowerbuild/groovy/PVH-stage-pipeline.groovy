lock('StageDeployLock') {
	echo "begin the stages of deploy"
	// Start
	
	agent any

    environment {
    	ENV=stage
        NOTIFICATION_EMAIL = 'DL-PVH-QA-TEAM@siriuscom.com,MASS-PVH@siriuscom.com,DL-PVH-DEV-TEAM@siriuscom.com,RobbBishop@pvh.com,shatabdisharma@pvh.com,RussellMiller@pvh.com'
    }
	
	stages {
		stage('Stage 1 - PRE-Requisites') {
			steps {
				echo "disable rundeck jobs & notify"
				node('buildServer') {
					echo "disable rundeck jobs"
					waitUntil {
						echo ""
						echo "disable_upcoming_rundeck_jobs"
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh disable $ENV'''
					}
				}
				echo "email some message"
				emailext body: 'Stage Deploy is now beginning.  Check the Jenkins job log for more information.', subject: 'Stage build now beginning.', to: "${NOTIFICATION_EMAIL}", from: 'MASS-PVH@siriuscom.com'
			}
		}
		
		stage('Stage 2 - Build Job') {
			echo "Run the build"
			node('buildServer') {
				echo 'Now running Jenkins Build JOB'
				waitUntil {
					// current job to do this
        			build job: 'Build'
				}
			}
		}
	}
}

lock('StageDeployLock') {
	stages {
		stage('Stage 3 - Deployment') {
			steps {
				echo "Now running deployment tasks"
				node('buildServer') {
					waitUntil {
						echo 'Now running Jenkins Deploy JOB'
						build job: 'Deploy'
					}
					// create job to do this ONLY if it is not integrated as part of the WC/Stage deploy
					// echo "Now running Stage SQL"
					// waitUntil {
					// 	echo "Run_Stage_SQL"
					// 	sh '''. /home/b2cnaswc/.profile; cd /opt/IBM/WebSphere/CommerceServer80/wcbd/
					// 	./run_stage_sql.sh ${ENV_S}'''
					// }
				}
			}
		}
	}
}


lock('StageDeployLock') {
	stages {
		// ADD PREVOTY STAGE
		stage('Stage 4 - Prevoty File Sync') {
			echo "Run Prevoty sync to sync"
			node('buildServer') {
				echo "run the prevoty sync job from rundeck"
					waitUntil {
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV job#_for_ALL_CATALOG_INDEXING'''
					}
				}
		}
		
		stage('Stage 5 - Solr Sync') {
			echo "Run SOLR sync to sync and pre-process and search artifacts"
			node('buildServer') {
				echo "run the solr sync script"
				waitUntil {
					sh '''cd /opt/IBM/WebSphere/CommerceServer80/wcbd
					./syncSolr_stg.sh'''
				}
			}
		}


		stage('Stage 6 - Dataload & CC job') {
			node('buildServer') {
				echo "run the dataload script"
				waitUntil {
					sh '''cd /opt/IBM/WebSphere/CommerceServer80/wcbd/
					./syncDataload_stg.sh'''						
				}
			}
			node('stageServer') {
				echo "run the Commerce composer job"
				waitUntil {
					sh '''cd /opt/dataload/CommerceComposer
					chmod 755 run.sh
					./run.sh'''
				}
			}
			echo "Now starting up search and WC servers in parallel"
			parallel firstBranch: {
				echo "starting stage search servers"
				node('buildServer') {
					echo "Run_Restart_Stage_SearchServers"
					sh '''cd /home/wasadm/bin
					./run_search_restarts.sh $ENV'''
				}
			}, secondBranch: {
				echo "starting primary stage app servers"
				node('buildServer') {
					waitUntil {
						echo "Run_Restart_Stage_AppServers"
						sh '''cd /home/wasadm/bin
						./run_wc_restarts.sh $ENV'''
					}
				}
			},
			failFast: false
		}
	}
}

lock('StageDeployLock') {
	stages {	
		stage('Stage 7 - Post Deploy') {
			steps {
				echo "Now running post deployment steps for late night refresh for catalog indexing and index prop"
				node('buildServer') {
					echo "run the late night refresh rundeck job for catalog indexing - ALL CATALOG INDEXING (This was introduced to run SOLR for all stores)"
					waitUntil {
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV job#_for_ALL_CATALOG_INDEXING'''
					}
				}
				echo "Now running dynacache and akamai cache clear"
				node('buildServer') {
					echo "run the late night refresh job for dynacache clear"
					waitUntil {
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV job#_for_DYNACACHE_CLEAR'''						
					}
				}
				echo "email QA message"
				emailext body: 'Stage Deploy is now Done.  Begin Smoke Testing and QA automation testing. Check the Jenkins job log for more information.', subject: 'Stage Deploy Done. Begin Testing', to: "${NOTIFICATION_EMAIL}", from: 'MASS-PVH@siriuscom.com'
			}
		}
	}
}

input 'Ready to proceed (y/n)?'

lock('StageDeployLock') {
	stages {	
		stage('Stage 8 - POST VERIFICATION') {
			steps {
				echo "enable rundeck jobs & notify"
				node('buildServer') {
					echo "enable rundeck jobs"
					waitUntil {
						echo ""
						echo "enable_upcoming_rundeck_jobs"				
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh enable $ENV'''
					}
				}	
				echo "email some message"
				emailext body: 'Stage Deploy is now complete.  Send Communication to PVH Business Users regarding post deploy tasks – depending on the current deploy.', subject: 'Stage Deploy now complete.', to: "${NOTIFICATION_EMAIL}", from: 'MASS-PVH@siriuscom.com'
			}
		}
	}
}
