lock('PRODDeployLock') {
	echo "begin the stages of deploy"
	// Start
	
	agent any
	
	environment {
        ENV_P1 = 'prod-cell2'
		ENV_P2 = 'prod-cell1'
        NOTIFICATION_EMAIL = 'DL-PVH-QA-TEAM@siriuscom.com,MASS-PVH@siriuscom.com,DL-PVH-DEV-TEAM@siriuscom.com,RobbBishop@pvh.com,shatabdisharma@pvh.com,RussellMiller@pvh.com'
    }

	stages {
		stage('Stage 1 - Notify & Mark WebServer down') {
			steps {
				echo "email some message"
				emailext body: '$ENV_P1 Deploy is now beginning.  Check the Jenkins job log for more information.', subject: '$ENV_P1 Deploy now beginning.', to: "${NOTIFICATION_EMAIL}", from: 'MASS-PVH@siriuscom.com'				
				echo "Now running rundeck job to mark web server down"
				node('buildServer') {
					echo "Running job Webserver 02 DOWN for LB"
					waitUntil {
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV_P1 job#_for_Webserver_02_DOWN_for_LB'''
					}
				}
			}
		}
		
		stage('Stage 2 - disable jobs') {	
			node('buildServer') {
				echo "disable rundeck jobs"
				waitUntil {
					echo ""
					echo "disable_upcoming_rundeck_jobs"
					sh '''cd /home/wasadm/bin/rundeck
					./rundeck_trigger.sh disable $ENV_P1'''
				}
			}
		}

		stage('Stage 3 - PROD Deployment') {
			steps {
				echo "Now running prod deployment tasks"
				node('buildServer') {
					waitUntil {
						echo 'Now running Jenkins Deploy JOB'
						build job: 'Deploy'
					}
					// create job to do this ONLY if it is not integrated as part of the WC/Stage deploy
					// echo "Now running Stage SQL"
					// waitUntil {
					// 	echo "Run_Stage_SQL"
					// 	sh '''. /home/b2cnaswc/.profile; cd /opt/IBM/WebSphere/CommerceServer80/wcbd/
					// 	./run_stage_sql.sh ${ENV_S}'''
					// }
				}
			}
		}
	}
}



lock('PRODDeployLock') {
	stages {
		stage('Stage 4 - Prevoty File Sync') {
			echo "Run Prevoty sync to sync"
			node('buildServer') {
				echo "run the prevoty sync job from rundeck"
					waitUntil {
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV job#_for_ALL_CATALOG_INDEXING'''
					}
				}
		}
		
		// ADD PREVOTY STAGE

		stage('Stage 5 - Solr Sync') {
			echo "Run SOLR sync to sync and pre-process and search artifacts"
			node('buildServer') {
				echo "run the solr sync script"
				waitUntil {
					sh '''cd /opt/IBM/WebSphere/CommerceServer80/wcbd
					./syncSolr_Prd_Cell02.sh'''
				}
			}
		}
		
		stage('Stage 6 - Full Propagation') {
			steps {
				echo "Full prop jobs followed by restarts & Testing notification"
				node('buildServer') {
					echo "FULL PROPAGATION (Runs stageprop and index propagation, and PowerReviews web server sync to Prod)"
					waitUntil {
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV_P1 143fc10e-ff78-4527-916f-fa2b6e36dd7d'''
					}
				}
				echo "ONLY After full prop is done, run restarts for search and wc in parallel to activate any changes made for SOLR Indexing"
				parallel firstBranch: {
					echo "starting search servers"
					node('buildServer') {
						echo "Run_Restart_Stage_SearchServers"
						sh '''cd /home/wasadm/bin
						./run_search_restarts.sh $ENV_P1'''
					}
				}, secondBranch: {
					echo "starting WC app servers"
					node('buildServer') {
						waitUntil {
							echo "Run_Restart_WC_AppServers"
							sh '''cd /home/wasadm/bin
							./run_wc_restarts.sh $ENV_P1 '''
						}
					}
				},
				failFast: false
			}
		}	
	}
}

input 'Ready to proceed with post deploy late night refresh for indexing and clearing cache?'

lock('PRODDeployLock') {
	stages {
		stage('Stage 7 - Post Deploy Steps - 3:05 AM') {
			steps {
				echo "Now running post deployment steps for catalog indexing followed by cache clear"
				// in PROD
				node('buildServer') {
					echo "PROD LATE NIGHT REFRESH: CATALOG INDEXING AND INDEXPROP - To account for promotions activating at 3am, this job runs all the catalog indexes, and then Index Prop"
					waitUntil {
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV_P1 9609876a-85e1-458a-a1f8-a85ec050012b'''
					}
					echo "After LATE NIGHT REFRESH, clear dynacache using  LATE NIGHT REFRESH: REGISTRY + DYNACACHE + AKAMAI CACHE CLEAR "
					waitUntil {
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV_P1 9609876a-85e1-458a-a1f8-a85ec050012b'''					
					}
				}
				echo "email some message"
				emailext body: '$ENV_P1 deploy is now ready for testing.  Begin Smoke Testing and QA automation testing for $ENV_P1. Check the Jenkins job log for more information.', subject: '$ENV_P1 Deploy ready for testing. Begin Testing', to: "${NOTIFICATION_EMAIL}", from: 'MASS-PVH@siriuscom.com'
			}
		}
	}
}

input 'Ready to proceed with marking web server up & enabling jobs?'

lock('PRODDeployLock') {
	stages {
		stage('Stage 8 - Post Verification Steps') {
			steps {
				echo "Now running post verification steps to enable web server back, resume rundeck jobs & notify business"
				node('webserver2') {
					echo "Mark the web server 2 up for LB for all 4 stores incl CK CAN now by using Run deck job Webserver 02 UP for LB "
					echo ""
					sh '''cd /home/wasadm/bin/rundeck
					./rundeck_trigger.sh execute $ENV_P1 job#_for_Webserver_02_UP_for_LB'''
				}
				node('buildServer') {
					echo "enable rundeck jobs"
					waitUntil {
						echo ""
						echo "enable_upcoming_rundeck_jobs"
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh enable $ENV_P1'''
					}
				}
				echo "email some message"
				emailext body: '$ENV_P1 deploy is now complete. Send Communication to PVH Business Users regarding post deploy tasks – depending on the current deploy.', subject: '$ENV_P1 Deploy now complete.', to: "${NOTIFICATION_EMAIL}", from: 'MASS-PVH@siriuscom.com'
			}
		}
	}
}

input 'Ready to proceed to next Cell?'

lock('PRODDeployLock') {
	stages {				
		stage('Stage 9 - notification & mark servers down') {
			steps {
				echo "email CELL 1 start message and mark web servers down"
				emailext body: '$ENV_P2 deploy is now beginning for CELL1.  Check the Jenkins job log for more information.', subject: '$ENV_P2 DEPLOY now beginning.', to: "${NOTIFICATION_EMAIL}", from: 'MASS-PVH@siriuscom.com'
				node('buildServer') {
					waitUntil {
						echo "Mark the web server 1 down for LB for all 4 stores (incl CK CAN now)"
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV_P2 job#_for_Webserver_01_DOWN_for_LB'''			
					}
				}
				node('buildServer') {
					waitUntil {
						echo "Mark the web server 3 down for LB for all 4 stores (incl CK CAN now)"
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV_P2 job#_for_Webserver_03_DOWN_for_LB'''			
					}		
				}
				node('buildServer') {
					waitUntil {
						echo "clear Akamai cache using rundeck job AKAMAI CACHE CLEAR - ALL STORES"
						// script is ready - job to do this has to be setup
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV_P2 88ec648c-8322-4f57-a708-65bc4e8f0fd5'''			
					}
				}
			}
		}

		stage('Stage 10 - Deployment') {
			echo "Now running prod deployment tasks for cell1"
			node('buildServer') {
				waitUntil {
					echo 'Now running Jenkins Deploy JOB'
					build job: 'Deploy'
				}
			}
		}
	}
}

lock('PRODDeployLock') {
	stages {		

		// ADD PREVOTY STAGE
		// ADD PREVOTY STAGE
		stage('Stage 11 - Prevoty File Sync') {
			echo "Run Prevoty sync to sync"
			node('buildServer') {
				echo "run the prevoty sync job from rundeck"
					waitUntil {
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV job#_for_ALL_CATALOG_INDEXING'''
					}
				}
		}

		stage('Stage 12 - SOLR & Dataload Sync') {
			echo "Run SOLR sync to sync and pre-process and search artifacts"
			node('buildServer') {
				echo "Run_SOLR_SYNC"
				sh '''cd /opt/IBM/WebSphere/CommerceServer80/wcbd
				./syncSolr_Prd_Cell01.sh'''
			}
			node('buildServer') {
				echo "run the dataload script"
				waitUntil {
					sh '''cd /opt/IBM/WebSphere/CommerceServer80/wcbd/
					./syncDataload_prod.sh'''
				}
			}
		}
		
		stage('Stage 13 - Restarts & Test notification') {
			steps {
				echo "CELL1 restarts"
				echo "running restarts for search and wc in parallel for CELL1"
				parallel firstBranch: {
					echo "starting search servers"
					node('buildServer') {
						echo "Run_Restart_Cell1_SearchServers"
						sh '''cd /home/wasadm/bin
						./run_cell1_search_restarts.sh $ENV_P2'''
					}
				}, secondBranch: {
					echo "starting WC app servers"
					node('buildServer') {
						waitUntil {
							echo "Run_Restart_Cell1_WC_AppServers"
							sh '''cd /home/wasadm/bin
							./run_cell1_wc_restarts.sh $ENV_P2'''
						}
					}
				},
				failFast: false			
				echo "email some message"
				emailext body: '$ENV_P2 deploy is ready for testing. Begin Smoke Testing and QA automation testing for CELL1. Check the Jenkins job log for more information.', subject: '$ENV_P2 Deploy Ready for testing. Begin Testing', to: "${NOTIFICATION_EMAIL}", from: 'MASS-PVH@siriuscom.com'
			}
		}
	}
}

input 'Ready to proceed with cache clear, bringing web servers back up & enabling jobs?'

lock('PRODDeployLock') {
	stages {
		stage('Stage 14 - Clear Akamai Cache') {	
			node('buildServer') {
				waitUntil {
					echo "clear Akamai cache using rundeck job AKAMAI CACHE CLEAR - ALL STORES"
					// script is ready - job to do this has to be setup
					sh '''cd /home/wasadm/bin/rundeck
					./rundeck_trigger.sh execute $ENV_P2 88ec648c-8322-4f57-a708-65bc4e8f0fd5'''			
				}
			}
		}

		stage('Stage 15 - POST VERIFICATION') {
			steps {
				echo "Now running post verification steps to enable web server back, resume rundeck jobs & notify business"
				node('buildServer') {
					waitUntil {
						echo "Mark the web server 1 up for LB for all 4 stores (incl CK CAN now)"
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV_P2 job#_for_Webserver_01_UP_for_LB'''			
					}
				}
				node('buildServer') {
					waitUntil {
						echo "Mark the web server 3 up for LB for all 4 stores (incl CK CAN now)"
						echo ""
						sh '''cd /home/wasadm/bin/rundeck
						./rundeck_trigger.sh execute $ENV_P2 job#_for_Webserver_03_UP_for_LB'''			
					}		
				}
				node('buildServer') {
					echo "enable rundeck jobs that were disabled for PROPAGATION"
					waitUntil {
						echo ""
						echo "enable_upcoming_rundeck_jobs"
						sh '''cd /home/wasadm/bin/rundeck
							./rundeck_trigger.sh enable $ENV_P2
						done'''
					}
				}
				echo "email some message"
				emailext body: '$ENV_P2 deploy is now complete.  Send Communication to PVH Business Users regarding post deploy tasks – depending on the current deploy.', subject: '$ENV_P2 Deploy CELL 1 now complete.', to: "${NOTIFICATION_EMAIL}", from: 'MASS-PVH@siriuscom.com'
			}
		}
	}
} 
