#!/bin/bash
#
# Copyright 2017, Sirius
#
# Created 2017-12-01 by Kaleeswaran Kalimuthu <kaleeswaran.kalimuthu@siriuscom.com>
#
# Description:
# This script was created to enable, disable and execute the rundeck job by terminal .


############ FUNCTIONS ####################

print_help() {

	echo -e "Usage :"
	echo -e " $PROGNAME --enable ENV"
	echo -e " $PROGNAME --disable ENV"
	echo -e " $PROGNAME --execute ENV EXECUTE_JOB_ID"
	exit 1
}

disableRundeckJobs() {

	echo -e "Disabling the deployment depend rundeck jobs"
	JOBID=$1

	if [[ ! -z $JOBID ]]; then
		
		curl -H "X-RunDeck-Auth-Token:$RUNDECK_AUTH_TOKEN" -X POST "${RUNDECK_API_URL}/job/$JOBID"/execution/disable > output.txt

		STATUS=`cat output.txt | cut -c10-13`

		if [[ $STATUS -eq "true" ]]; then
			echo -e "$JOBID - Disabled"
		else
			echo -e "$JOBID - Disable was not successfully , Please disable manually !!"
		fi
	else
		echo -e "Missing parameters JOBID"
	fi

}

enableRundeckJobs() {

	echo -e "Enabling the deployment depend rundeck jobs"
	JOBID=$1
	OUTPUT=""

	if [[ ! -z $JOBID ]]; then
		
		curl -H "X-RunDeck-Auth-Token:$RUNDECK_AUTH_TOKEN" -X POST "${RUNDECK_API_URL}/job/$JOBID"/execution/enable > output.txt

		STATUS=`cat output.txt | cut -c10-13`
		
		if [[ $STATUS -eq "true" ]]; then
			echo -e "$JOBID - Enabled"
		else
			echo -e "$JOBID - Enable was not successfully , Please enable manually !!"
		fi
	else
		echo -e "Missing parameters JOBID"
	fi

}

executeRundeckJobs() {
	
	JOBID=$1
	echo -e "Executing the jobs by $JOBID"

	if [[ ! -z $JOBID ]]; then
		curl -H "X-RunDeck-Auth-Token:$RUNDECK_AUTH_TOKEN" -X POST "${RUNDECK_API_URL}/job/$JOBID/run" > execute_status.xml
		EXEC_ID=`grep "<execution id=" execute_status.xml | awk '{print $2}' | grep -o "[0-9]\+"`
		echo "Job execution id is $EXEC_ID"
		if [[ ! -z $EXEC_ID ]]; then
			EXEC_STATUS=1
			while [[ $EXEC_STATUS -eq 1 ]]; do
				curl -H "X-RunDeck-Auth-Token:$RUNDECK_AUTH_TOKEN" -X GET "${RUNDECK_API_URL}/execution/$EXEC_ID" > execute_status.xml
				STATUS=`grep -wo 'succeeded\|failed\|running\|killed\|aborted' execute_status.xml`

				echo "Job execution status is $STATUS"

				if [[ "$STATUS" == "succeeded" ]]; then
					echo "JOBID - $JOBID - Successfully completed."
					EXEC_STATUS=0
				elif [[ "$STATUS" == "failed" ]] || [[ "$STATUS" == "killed" ]] || [[ "$STATUS" == "aborted" ]]; then
					echo "JOBID - $JOBID - Failed or Killed"
					EXEC_STATUS=0
					# -> Need to confirm whether we need to send any email confirmation to the team or not
				elif [[ "$STATUS" == "running" ]]; then
					echo "Still running - $JOBID"
					EXEC_STATUS=1
					sleep 60s
				else
					echo "JOBID - $JOBID is in unknown state, Please review the logs and make sure the process is running fine!!"
					EXEC_STATUS=0
				fi
			done
		else
			echo -e "JOBID-$JOBID was not successfully executed,It might be already running!!"
		fi
	fi

}

############ MAIN #######################

DIR=/home/wasadm/bin/pipeline/conf

CURRENT_TIME=`date`
echo -e "Execution started at $CURRENT_TIME"

PROGNAME=`basename $0`
if [[ $# -lt 2 ]]; then
	echo -e "Please pass at least two arguments required for the execution of the script - the task and the environment and if required the job_id as well"
	print_help
else 
	TASK=$1
	ENV=$2
fi

ENV=`echo $ENV | tr [:upper:] [:lower:]`
TASK=`echo $TASK | tr [:upper:] [:lower:]`

if [[ $# -eq 3 ]]; then
	execute_job_uid=$3
fi

case $ENV in
	local|stage|prod_cell2|prod_cell1|dr_stage|dr_cell1|dr_cell2 )
		if [[ ! -f $DIR/$ENV.conf ]]; then
			echo -e "Missing configuration file ."
			exit 1
		else
			. $DIR/$ENV.conf
		fi
		;;
	* )
		echo "check the ENV variable passed and verify the approproate configuration file exists"
esac

if [[ -z $TASK ]]; then
	echo -e "Please specify the parameters to enable/disable/execute"
	print_help
fi

case $TASK in
	--enable)
		for job_uid in "${RUNDECK_ENABLE_JOB_IDS[@]}" ; do
			enableRundeckJobs $job_uid
		done
		;;
	--disable)
		for job_uid in "${RUNDECK_DISABLE_JOB_IDS[@]}" ; do
			disableRundeckJobs $job_uid
		done
		;;
	--execute)
		executeRundeckJobs $execute_job_uid
		;;
	* )
		echo -e "Please specify the parameters to enable/disable/execute"
		print_help
esac
