#!/bin/bash
#
# Copyright 2017, Sirius
#
# Created 2017-12-06 by Kaleeswaran Kalimuthu <kaleeswaran.kalimuthu@siriuscom.com>
#
# Description:
# This script was created to restart servers on all the app tiers.

############ FUNCTIONS ####################

print_help() {

	echo -e "Usage :"
	echo -e " $PROGNAME EnvName"
	exit 1
}

CURRENT_TIME=`date`
echo -e "Execution started at $CURRENT_TIME"

PROGNAME=`basename $0`

# GET ENV DETAILS
if [[ $# -lt 1 ]]; then
	echo -e "Please pass at least one arguments required for the execution of the script - the environment is required to run this script"
	print_help
else 
	ENV=$1
fi
# CONVERT TO LOWER CASE 
ENV=`echo $ENV | tr [:upper:] [:lower:]`
# LOAD ENV SPECIFIC CONF FILE
if [[ ! -f /home/wasadm/bin/pipeline/conf/${ENV}.conf ]]; then
	echo -e "Missing pipeline.conf file. Please check and re-run!!"
	exit 1
else
	. /home/wasadm/bin/pipeline/conf/${ENV}.conf
fi
# PREVOTY LOGIC TO SCP files to all cluster members
echo "Envname $ENV"
case $ENV in
	psqa|dev2|dev3|stgqa|prdqa|dr_stage|dr_cell01|dr_cell02|stage|prod_cell02|prod_cell01 )
		echo -e "Constructing server cluster array name"
		ENV_serverCluster_nw="${ENV}_serverCluster"[@]
		ENV_serverCluster=( "${!ENV_serverCluster_nw}" )
		
		for wcsServer in "${!ENV_serverCluster[@]}"
		do
			wcsServer="${ENV_serverCluster[$wcsServer]}"
			echo -e "Logging into $wcsServer now"
			ssh wasadm@$wcsServer </dev/null <<EOF 2>&1
	
			echo "restart WCS servers"




			echo "restart SOLR servers"






EOF
			continue
		done
		;;
	* )
		echo "check the ENV variable passed and verify the appropriate configuration file exists"
esac
